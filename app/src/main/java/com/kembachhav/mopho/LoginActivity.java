package com.kembachhav.mopho;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

public class LoginActivity extends AppCompatActivity {
    String[] SPINNERLIST = {"Admin","Distributor","User"};
    String pass,mail;
     EditText etmail,etpassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button signin=findViewById(R.id.btnSignin);
        TextView tv= findViewById(R.id.link_to_signup);

        etmail=findViewById(R.id.et_email);
        etpassword=findViewById(R.id.et_password);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, SPINNERLIST);
        MaterialBetterSpinner materialDesignSpinner = (MaterialBetterSpinner) findViewById(R.id.sp_user);
        materialDesignSpinner.setAdapter(arrayAdapter);


        materialDesignSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String selectedItemText = (String) parent.getItemAtPosition(position);


                    Toast.makeText(getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT).show();

                    Toast.makeText(getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT).show();


               /* switch(position) {

                    case 0 : // for item 1
                        Intent intent= new Intent(LoginActivity.this, RegistrationActivity.class);
                        startActivity(intent);
                        break;

                    case 1 :
                        Intent intent1= new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent1);
                        break;

                    *//* you can have any number of case statements *//*
                    default :

                }*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginActivity.this,RegistrationActivity.class);
                startActivity(intent);
            }
        });

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(etmail.getText().toString())) {
                    etmail.setError("Enter Email/Mobile Number");
                    return;
                }
                if(TextUtils.isEmpty(etpassword.getText().toString())) {
                    etpassword.setError("Enter Valid Password");
                    return;
                }
                Intent intent=new Intent(LoginActivity.this,SelectBrand.class);
                startActivity(intent);
            }
        });
    }
}
