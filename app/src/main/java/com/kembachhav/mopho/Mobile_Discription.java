package com.kembachhav.mopho;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.kembachhav.mopho.Model.AddMobileModel;

import java.util.List;

public class Mobile_Discription extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile__discription);
        Button signin=findViewById(R.id.buy_now);
        Button addCart=findViewById(R.id.add_to_cart);
        List<AddMobileModel> MobileList = null;



        /*AddMobileModel studentDetails = new AddMobileModel();
        Mname.setText(studentDetails.getBrand());
        Mmodel.setText(studentDetails.getModel());
        Mcolor.setText(studentDetails.getStorage());
        Mprice.setText(studentDetails.getPrice());
*/
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Mobile_Discription.this,DeliveryActivity.class);
                startActivity(intent);
            }
        });

        addCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Mobile_Discription.this,HomeActivity.class);
                startActivity(intent);
            }
        });
    }
}
