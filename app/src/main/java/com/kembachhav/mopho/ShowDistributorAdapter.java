package com.kembachhav.mopho;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class ShowDistributorAdapter extends RecyclerView.Adapter<ShowDistributorAdapter.MyViewHolder>{

    private String[] Name,Model,Color,Price;

    public ShowDistributorAdapter(String[] name, String[] model, String[] color, String[] price) {
        Name = name;
        Model = model;
        Color = color;
        Price = price;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_home, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.Mname.setText(Name[position]);
        holder.Mmodel.setText(Model[position]);
        holder.Mcolor.setText(Color[position]);
        holder.Mprice.setText(Price[position]);
    }

    @Override
    public int getItemCount() {
        return Name.length;
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView Mname, Mmodel, Mcolor,Mprice;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            Mname = (TextView) itemView.findViewById(R.id.mName);
            Mmodel = (TextView) itemView.findViewById(R.id.mModel);
            Mcolor = (TextView) itemView.findViewById(R.id.mColor);
            Mprice = (TextView) itemView.findViewById(R.id.mPrice);

            // set click event
            itemView.setOnClickListener(this);
            Mname.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == Mname.getId()) {
                Toast.makeText(v.getContext(), "ITEM PRESSED = " + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(v.getContext(), "ROW PRESSED = " + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
