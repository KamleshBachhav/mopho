package com.kembachhav.mopho;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

public class RegistrationActivity extends AppCompatActivity {
    String[] SPINNERLIST = {"Admin","Distributor","User"};
    EditText email,password,mobile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        email=findViewById(R.id.email);
        password=findViewById(R.id.password);
        mobile=findViewById(R.id.mobile);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, SPINNERLIST);
        MaterialBetterSpinner materialDesignSpinner = (MaterialBetterSpinner) findViewById(R.id.sp_user1);
        materialDesignSpinner.setAdapter(arrayAdapter);

        TextView tvf= findViewById(R.id.btnLinkToSignin);
        tvf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RegistrationActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });

        Button singup = findViewById(R.id.btnSignup);
        singup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(TextUtils.isEmpty(email.getText().toString())) {
                    email.setError("Enter Email Number");
                    return;
                }
                if(TextUtils.isEmpty(mobile.getText().toString())) {
                    mobile.setError("Enter Mobile Number");
                    return;
                }
                if(TextUtils.isEmpty(password.getText().toString())) {
                    password.setError("Enter Valid Password");
                    return;
                }

                Intent intent=new Intent(RegistrationActivity.this,Distri_Basic_info.class);
                intent.putExtra("Email",email.getText().toString());
                intent.putExtra("Mobile",mobile.getText().toString());
                startActivity(intent);
            }
        });


    }
}
