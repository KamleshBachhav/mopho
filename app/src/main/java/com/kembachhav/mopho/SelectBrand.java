package com.kembachhav.mopho;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SelectBrand extends AppCompatActivity {

    TextView oppo,one,samsung,vivo,xiomi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_brand);

        oppo=findViewById(R.id.oppo);
        one=findViewById(R.id.one);
        samsung=findViewById(R.id.samsung);
        vivo=findViewById(R.id.vivo);
        xiomi=findViewById(R.id.xiomi);

        oppo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SelectBrand.this,HomeActivity.class);
                startActivity(intent);
            }
        });

        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SelectBrand.this,HomeActivity.class);
                startActivity(intent);
            }
        });


        samsung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SelectBrand.this,HomeActivity.class);
                startActivity(intent);
            }
        });

        vivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SelectBrand.this,HomeActivity.class);
                startActivity(intent);
            }
        });

        xiomi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SelectBrand.this,HomeActivity.class);
                startActivity(intent);
            }
        });
    }
}
