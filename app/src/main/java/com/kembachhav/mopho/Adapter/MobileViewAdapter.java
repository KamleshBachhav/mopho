package com.kembachhav.mopho.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kembachhav.mopho.Mobile_Discription;
import com.kembachhav.mopho.Model.AddMobileModel;
import com.kembachhav.mopho.R;

import java.util.List;

public class MobileViewAdapter extends RecyclerView.Adapter<MobileViewAdapter.MyViewHolder> {

    Context context;
    List<AddMobileModel> MobileList;

    public MobileViewAdapter(Context context, List<AddMobileModel> mobileList) {
        this.context = context;
        MobileList = mobileList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_home, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        AddMobileModel studentDetails = MobileList.get(position);
        holder.Mname.setText(studentDetails.getBrand());
        holder.Mmodel.setText(studentDetails.getModel());
        holder.Mcolor.setText(studentDetails.getStorage());
        holder.Mprice.setText(studentDetails.getPrice());
        //Loading image from Glide library.
       // Glide.with(context).load(studentDetails.getImageURL()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {

        return MobileList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView Mname, Mmodel, Mcolor,Mprice;
        AppCompatImageView imageView;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView=(AppCompatImageView) itemView.findViewById(R.id.img_mobile);
            Mname = (TextView) itemView.findViewById(R.id.mName);
            Mmodel = (TextView) itemView.findViewById(R.id.mModel);
            Mcolor = (TextView) itemView.findViewById(R.id.mColor);
            Mprice = (TextView) itemView.findViewById(R.id.mPrice);

            // set click event
            itemView.setOnClickListener(this);
            Mname.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final Intent intent;
            switch (Mname.getId()){
                case 0:
                    intent =  new Intent(context, Mobile_Discription.class);
                    break;
                case 1:
                    intent =  new Intent(context, Mobile_Discription.class);
                    break;
                default:
                    intent =  new Intent(context, Mobile_Discription.class);
                    break;
            }
            context.startActivity(intent);
        }
    }
}
