package com.kembachhav.mopho;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kembachhav.mopho.Model.AddMobileModel;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.IOException;
import java.util.UUID;

public class Add_Mobile extends AppCompatActivity {

    private MaterialEditText desc, price;
    private MaterialEditText brand;
    private MaterialEditText model;
    private MaterialEditText storage;
    private Button add;
    private LinearLayoutManager linearLayoutManager;

    // Folder path for Firebase Storage.
    String Storage_Path = "Mopho/";

    // Root Database Name for Firebase Database.
    public static final String Database_Path = "Mobiles";

    // Creating button.
    Button ChooseButton;
    // Creating EditText.
    EditText ImageName ;
    // Creating ImageView.
    ImageView SelectImage;
    // Creating URI.
    Uri FilePathUri;
    int Image_Request_Code = 7;
    ProgressDialog progressDialog ;
    // Creating StorageReference and DatabaseReference object.
    StorageReference storageReference;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__mobile);
        FirebaseStorage storagee = FirebaseStorage.getInstance();
        storageReference = storagee.getReference();
        //Assign ID'S to button.
        ChooseButton = (Button)findViewById(R.id.ButtonChooseImage);
        ImageName = (EditText)findViewById(R.id.ImageNameEditText);
        SelectImage = (ImageView)findViewById(R.id.ShowImageView);

        desc = findViewById(R.id.editText);
        price = findViewById(R.id.Price);
        add = findViewById(R.id.btnAdd);
        storage = findViewById(R.id.spinner_Storage);
        brand =  findViewById(R.id.spinner_Mobile);
        model =  findViewById(R.id.spinner_Model);

        // Adding click listener to Choose image button.
        ChooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Creating intent.
                Intent intent = new Intent();
                // Setting intent type as image to select image from phone storage.
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Please Select Image"), Image_Request_Code);

            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Calling method to upload selected image on Firebase storage.
                UploadImageFileToFirebaseStorage();
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Mobiles").push();
                AddMobileModel add = new AddMobileModel();
                add.setBrand(brand.getText().toString());
                add.setModel(model.getText().toString());
                add.setDiscription(desc.getText().toString());
                add.setPrice(price.getText().toString());
                add.setStorage(storage.getText().toString());
                databaseReference.setValue(add);
                Toast.makeText(Add_Mobile.this,"Mobile Added Successfully", Toast.LENGTH_SHORT).show();

                        Intent intent=new Intent(Add_Mobile.this,HomeActivity.class);
                        startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Image_Request_Code && resultCode == RESULT_OK && data != null && data.getData() != null) {
            FilePathUri = data.getData();
            try {
                // Getting selected image into Bitmap.
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), FilePathUri);
                // Setting up bitmap selected image into ImageView.
                SelectImage.setImageBitmap(bitmap);
                // After selecting image change choose button above text.
                ChooseButton.setText("Image Selected");
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // Creating UploadImageFileToFirebaseStorage method to upload image on storage.
    public void UploadImageFileToFirebaseStorage() {

        // Checking whether FilePathUri Is empty or not.
        if (FilePathUri != null) {
            progressDialog = new ProgressDialog(this);
            // Setting progressDialog Title.
           progressDialog.setMessage("Image is Uploading...");

            // Showing progressDialog.
            progressDialog.show();

            // Creating second StorageReference.
            StorageReference storageReference2nd = storageReference.child("Mopho/" + UUID.randomUUID().toString());

            // Adding addOnSuccessListener to second StorageReference.
            storageReference2nd.putFile(FilePathUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            // Getting image name from EditText and store into string variable.
                            String TempImageName = ImageName.getText().toString().trim();

                            // Hiding the progressDialog after done uploading.
                            progressDialog.dismiss();

                            // Showing toast message after done uploading.
                            Toast.makeText(getApplicationContext(), "Image Uploaded Successfully ", Toast.LENGTH_LONG).show();

                            @SuppressWarnings("VisibleForTests")
                            AddMobileModel imageUploadInfo = new AddMobileModel(TempImageName, taskSnapshot.getUploadSessionUri().toString());

                            // Getting image upload ID.
                            String ImageUploadId = databaseReference.push().getKey();

                            // Adding image upload id s child element into databaseReference.
                            databaseReference.child(ImageUploadId).setValue(imageUploadInfo);
                        }
                    })
                    // If something goes wrong .
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {

                            // Hiding the progressDialog.
                            progressDialog.dismiss();

                            // Showing exception erro message.
                            Toast.makeText(Add_Mobile.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })

                    // On progress change upload time.
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            // Setting progressDialog Title.
                            progressDialog.setTitle("Image is Uploading...");

                        }
                    });
        }
        else {

            Toast.makeText(Add_Mobile.this, "Please Select Image or Add Image Name", Toast.LENGTH_LONG).show();

        }
    }

}
