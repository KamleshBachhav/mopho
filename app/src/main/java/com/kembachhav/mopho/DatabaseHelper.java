package com.kembachhav.mopho;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Kem on 3/26/2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "Webvision.db";
    public static final String Location = "Location";
    public static final String PositionRI = "PositionRI";
    public static final String TrackType = "TrackType";
    public static final String PEAKS = "Peaks";
    public static final String ID = "ID";
    public static final String LOCATION_START = "LOCATION_START";
    public static final String LOCATION_END = "LOCATION_END";
    public static final String DIST_COVER = "DIST_COVER";
    public static final String COL_5 = "VERTICAL";
    public static final String COL_6 = "LATERAL";
    public static final String SPEED = "SPEED";
    public static final String BL_VERTICAL = "BL_VERTICAL";
    public static final String BL_LATERAL = "BL_LATERAL";
    public static final String KM_VERTICALRI = "KM_VERTICALRI";
    public static final String KM_LATERALRI = "KM_LATERALRI";
    public static final String COL_12 = "SPEED";
    public static final String RUN_NO = "RUN_NO";
    public static final String TRAIN_NO = "TRAIN_NO";
    public static final String RAILWAY = "RAILWAY";
    public static final String TYPE = "TYPE";
    public static final String COUCH_NO = "COUCH_NO";
    public static final String COUCH_TYPE = "COUCH_TYPE";
    public static final String START = "START";
    public static final String ENDD = "ENDD";
    public static final String TIME = "TIME";
    public static final String VERTICAL_TH = "VERTICAL_TH";
    public static final String LATERAL_TH = "LATERAL_TH";
    public static final String RUN_ID = "RUN_ID";
    public static final String VERTICAL = "VERTICAL_PEAK";
    public static final String LATERAL = "LATERAL_PEAK";
    public static final String LONGITUDE = "LONGITUDE ";
    public static final String LATITUDE = "LATITUDE";
    public static final String BLOCK = "BLOCK";
    public static final String CREATED = "CREATED";
    public static final String INTERVAL = "INTERVAL";
    public static final String KM_COVERED = "KM_COVERED";
    public static final String VER_ACTION = "VER_ACTION";
    public static final String LAT_ACTION = "LAT_ACTION";
    public static final String ROUTE_NAME = "ROUTE_NAME";
    public static final String ROUTE_KM = "ROUTE_KM";
    public static final String ROUTE_METER = "ROUTE_METER";
    public static final String ROUTE_TYPE = "ROUTE_TYPE";
    public static final String ROUTE = "ROUTE";

    SQLiteDatabase db;
    public static Context currentContext;
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        currentContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TrackType +" (ID INTEGER PRIMARY KEY AUTOINCREMENT,RUN_NO INTEGER,TRAIN_NO FLOAT,RAILWAY CHARACTER,TYPE CHARACTER,COUCH_NO FLOAT,COUCH_TYPE CHARACTER,START FLOAT,ENDD FLOAT,VERTICAL_TH FLOAT,LATERAL_TH FLOAT,INTERVAL FLOAT, CREATED DATETIME DEFAULT CURRENT_TIMESTAMP)");
        db.execSQL("create table " + Location +" (ID INTEGER PRIMARY KEY AUTOINCREMENT,LOCATION_START FLOAT,LOCATION_END FLOAT,DIST_COVER FLOAT,SPEED FLOAT,VERTICAL FLOAT,LATERAL FLOAT,LONGITUDE FLOAT,LATITUDE FLOAT,TIME DATETIME DEFAULT CURRENT_TIMESTAMP,RUN_ID LONG, FOREIGN KEY (RUN_ID) REFERENCES TrackType(ID))");
        db.execSQL("create table " + PEAKS +" (ID INTEGER PRIMARY KEY AUTOINCREMENT,VERTICAL_PEAK FLOAT,LATERAL_PEAK FLOAT,LONGITUDE FLOAT,LATITUDE FLOAT, BLOCK INTEGER DEFAULT 1,CREATED DATETIME DEFAULT CURRENT_TIMESTAMP)");
        db.execSQL("create table " + PositionRI +" (ID INTEGER PRIMARY KEY AUTOINCREMENT,LOCATION_START FLOAT,LOCATION_END FLOAT,BLOCK FLOAT,KM_COVERED,BL_VERTICAL FLOAT,BL_LATERAL FLOAT,KM_VERTICALRI,KM_LATERALRI,VER_ACTION,LAT_ACTION,SPEED FLOAT,TIME DATETIME DEFAULT CURRENT_TIMESTAMP,RUN_ID LONG, FOREIGN KEY (RUN_ID) REFERENCES TrackType(ID))");
        db.execSQL("create table " + ROUTE +" (ID INTEGER PRIMARY KEY AUTOINCREMENT,ROUTE_NAME TEXT,ROUTE_KM FLOAT,ROUTE_METER FLOAT,ROUTE_TYPE TEXT,CREATED TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ Location);
        db.execSQL("DROP TABLE IF EXISTS "+ PositionRI);
        db.execSQL("DROP TABLE IF EXISTS "+ TrackType);
        db.execSQL("DROP TABLE IF EXISTS "+PEAKS);
        db.execSQL("DROP TABLE IF EXISTS "+ROUTE);
        onCreate(db);
    }

    public long insertData(String kmfrom, String kmto,String dist, String spid, String vert,String late,float longg,float lat,String run_id) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(LOCATION_START,kmfrom);
        contentValues.put(LOCATION_END,kmto);
        contentValues.put(DIST_COVER,dist);
        contentValues.put(SPEED,spid);
        contentValues.put(COL_5,vert);
        contentValues.put(COL_6,late);
        contentValues.put(LONGITUDE,longg);
        contentValues.put(LATITUDE,lat);
        contentValues.put(TIME,getDateTime());
        contentValues.put(RUN_ID,run_id);
        return db.insert(Location,null ,contentValues);
    }

    public boolean insertData1(String Kmfrom, String kmto, String block,String km_cover,String blverti, String bllater, String kmverti, String kmlater,String verAction, String latAction,String spiid,String run_id) {

        SQLiteDatabase db1 = this.getWritableDatabase();
        ContentValues contentValues1 = new ContentValues();
        contentValues1.put(LOCATION_START,Kmfrom);
        contentValues1.put(LOCATION_END,kmto);
        contentValues1.put(BLOCK,block);
        contentValues1.put(KM_COVERED,km_cover);
        contentValues1.put(BL_VERTICAL,blverti);
        contentValues1.put(BL_LATERAL,bllater);
        contentValues1.put(KM_VERTICALRI,kmverti);
        contentValues1.put(KM_LATERALRI,kmlater);
        contentValues1.put(VER_ACTION,verAction);
        contentValues1.put(LAT_ACTION,latAction);
        contentValues1.put(COL_12,spiid);
        contentValues1.put(TIME,getDateTime());
        contentValues1.put(RUN_ID,run_id);
        long result1 = db1.insert(PositionRI,null ,contentValues1);
        if(result1 == -1)
            return false;
        else
            return true;
    }
    public long insertData2(String Runno, String Train,String railway,String type, String couchno, String chtype,String start, String end, String vth, String lth, String interval)
    {
        SQLiteDatabase db1 = this.getWritableDatabase();
        ContentValues contentValues1 = new ContentValues();
        contentValues1.put(RUN_NO, Runno);
        contentValues1.put(TRAIN_NO,Train);
        contentValues1.put(RAILWAY,railway);
        contentValues1.put(TYPE,type);
        contentValues1.put(COUCH_NO,couchno);
        contentValues1.put(COUCH_TYPE, chtype);
        contentValues1.put(START,start);
        contentValues1.put(ENDD,end);
        contentValues1.put(VERTICAL_TH,vth);
        contentValues1.put(LATERAL_TH,lth);
        contentValues1.put(INTERVAL,interval);
        contentValues1.put(CREATED,getDateTime());
        return db1.insert(TrackType,null ,contentValues1);
    }

    public boolean insertPeak(String vertical, String lateral, String longitude, String latitude, Integer block)
    {
        SQLiteDatabase db1 = this.getWritableDatabase();
        ContentValues contentValues1 = new ContentValues();
        contentValues1.put(VERTICAL, vertical);
        contentValues1.put(LATERAL,lateral);
        contentValues1.put(LONGITUDE,longitude);
        contentValues1.put(LATITUDE,latitude);
        contentValues1.put(BLOCK,block);
        long result1 = db1.insert(PEAKS,null ,contentValues1);
        if(result1 == -1)
            return false;
        else
            return true;
    }
    public long route(String name, String km,String meter,String type)
    {
        SQLiteDatabase db1 = this.getWritableDatabase();
        ContentValues contentValues1 = new ContentValues();
        contentValues1.put(ROUTE_NAME, name);
        contentValues1.put(ROUTE_KM,km);
        contentValues1.put(ROUTE_METER,meter);
        contentValues1.put(ROUTE_TYPE,type);
        contentValues1.put(CREATED,getDateTime());
        return db1.insert(ROUTE,null ,contentValues1);
    }
    /**
     * Getting all labels
     * returns list of labels
     * */
    public List<String> getAllLabels(){
        List<String> labels = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT DISTINCT ROUTE_NAME FROM " + ROUTE;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                labels.add(cursor.getString(cursor.getColumnIndex("ROUTE_NAME")));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();
        // returning lables
        return labels;
    }
}