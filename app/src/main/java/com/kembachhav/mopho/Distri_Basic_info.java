package com.kembachhav.mopho;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kembachhav.mopho.Model.DistriBasicModel;

import butterknife.ButterKnife;

public class Distri_Basic_info extends AppCompatActivity {


    private EditText Name;
    private EditText Number;
    private RadioGroup radioGroup;
    private EditText Email,DOB,Address,City,State;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distri__basic_info);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        String em = intent.getStringExtra("Email");
        String mb = intent.getStringExtra("Mobile");

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radioGroup.clearCheck();
        Name=findViewById(R.id.et_name);
        Number=findViewById(R.id.et_mobile);
        Email=findViewById(R.id.et_Email);
                DOB=findViewById(R.id.et_dob);
                Address=findViewById(R.id.et_addr);
                City=findViewById(R.id.et_city);
                State=findViewById(R.id.et_state);

                Email.setText(em);
                Number.setText(mb);


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb) {
                    Toast.makeText(Distri_Basic_info.this, rb.getText(), Toast.LENGTH_SHORT).show();
                }

            }
        });

        Button save= findViewById(R.id.btn_save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Information").push();
                DistriBasicModel addd = new DistriBasicModel();
                addd.setName(Name.getText().toString());
                addd.setNumber(Number.getText().toString());
                addd.setEmail(Email.getText().toString());
                addd.setDob(DOB.getText().toString());
                addd.setAddress(Address.getText().toString());
                addd.setCity(City.getText().toString());
                addd.setState(State.getText().toString());
                databaseReference.setValue(addd);
                Toast.makeText(Distri_Basic_info.this,"Info Save Successfully", Toast.LENGTH_SHORT).show();

                Intent intent=new Intent(Distri_Basic_info.this,Add_Mobile.class);
                startActivity(intent);
            }
        });
    }
}
